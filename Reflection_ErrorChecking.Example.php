/**
*  Using reflection to handle error checking on missing data in method arguments
*  if $this->error ==1, then you can react and show the array of error messages from $this->errors
*
*  I am using this for an api endpoint and needed an easier way to show an error response when functions were missing
*
*/


Class Foo {
 
    public function __construct(){
        $this->error=0;
        $this->errors=array();
    }
 
 
    public function checkErrors($funcName,$arg_list){
                $r = new ReflectionMethod('Endpoint', $funcName);
                $params = $r->getParameters();
                foreach ($params as $key=> $param) {
                        if(!isset($arg_list[$key])){
                                $this->error=1;
                                $this->errors[]="Value for ".$param->getName(). " is not set.";
                        }
                }
        }
 
 
    public function foo($var1,$var2,$var3){
        $this->checkErrors(__FUNCTION__,func_get_args());
    }
 
 
 
 
 
}
